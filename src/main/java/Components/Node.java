package Components;
import Events.InitMessage;
import Events.MergeMessage;
import Events.ReportMessage;
import Ports.EdgePort;
import misc.TableRow;
import se.sics.kompics.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.stream.Stream;

import com.jramoyo.io.IndexedFileReader;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;


public class Node extends ComponentDefinition {
    Positive<EdgePort> recievePort = positive(EdgePort.class);
    Negative<EdgePort> sendPort = negative(EdgePort.class);
    Map<String, Integer> countByWords = new HashMap<String, Integer>();
    HashSet<String> srces = new HashSet<String>();
    Boolean isRoot = false;
    String nodeName;
    String parentName;
	boolean isLeaf;
    String add;
    ArrayList<String> children = new ArrayList<String>();
    int fragmentName = 0;
    int level = 0;
    int dist = 10000;

    HashMap<String,Integer> neighbours = new HashMap<>();
    ArrayList<TableRow> route_table = new ArrayList<>();

    Handler mergeHandler = new Handler<MergeMessage>(){
        @Override
        public void handle(MergeMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst)){
                System.out.println(nodeName +  " recieved message : src " + event.src + " dst " + event.dst);
            
            }
        }
    };


    Handler reportHandler = new Handler<ReportMessage>() {
        @Override
        public void handle(ReportMessage event) {
            if (nodeName.equalsIgnoreCase(event.dst))
            {
            	System.out.println("report from " + event.src + " to " + event.dst);
            	srces.add(event.src);
            	System.out.println("update");
            	updateCounts(event.countByWords);
            	for(String i : srces) {
            		System.out.println("\t" + i);
            	}
            	if(!isRoot && getAllSrcWithoutOne()) {
            		parentName = findParent();
                	trigger(new ReportMessage(nodeName,parentName,countByWords),sendPort);
            	} else if (isRoot && getAllSrc()) {
            		FileWriter fileWriter = null;
					try {
						fileWriter = new FileWriter("result.txt");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		PrintWriter printWriter = new PrintWriter(fileWriter);
            		for(String i : countByWords.keySet()) {
            			printWriter.println(i + ":" + countByWords.get(i));
                	}
            		printWriter.close();
            	}

            }
        }
    };
    
    private void updateCounts(Map<String, Integer> count) {
    	for(String i : count.keySet()) {
    		Integer num = countByWords.get(i);
            if (num != null) {
                countByWords.put(i, num + count.get(i));
            } else {
                countByWords.put(i, count.get(i));
            }
    	}
    }



    protected boolean getAllSrc() {
		// TODO Auto-generated method stub
    	if(srces.size() == neighbours.keySet().size() && neighbours.keySet().containsAll(srces)) {
			return true;
		}
		return false;
	}



	protected boolean getAllSrcWithoutOne() {
		// TODO Auto-generated method stub
		if(srces.size() == (neighbours.keySet().size()-1) && neighbours.keySet().containsAll(srces)) {
			return true;
		}
		return false;
	}



	Handler startHandler = new Handler<Start>() {
        @Override
        public void handle(Start event) {
            if (isLeaf) {
            	System.out.println("start at " + nodeName);
            	try {
					readFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	parentName = findParent();
            	System.out.println(parentName);
//            	for(String i : countByWords.keySet()) {
//            		System.out.println(i + " : " + countByWords.get(i));
//            	}
            	trigger(new ReportMessage(nodeName,parentName,countByWords),sendPort);
            }
        }
    };
    
    private String findParent() {
    	if(isLeaf) {
    		return neighbours.keySet().iterator().next();
    	} else if (isRoot) {
    		return "";
    	}
    	neighbours.keySet().removeAll(srces);
    	return neighbours.keySet().iterator().next();
    }
    
    private void count(String src) {
 	   Integer count = countByWords.get(src);
        if (count != null) {
            countByWords.put(src, count + 1);
        } else {
            countByWords.put(src, 1);
        }
    }
    
    private void readFile() throws IOException {
    	try (Stream<String> lines = Files.lines(Paths.get("target/" + add),
    			StandardCharsets.UTF_8))
    	{
    	    for (String line : (Iterable<String>) lines::iterator)
    	    {
    	    	StringTokenizer tokenizer = new StringTokenizer(line);
        		while(tokenizer.hasMoreTokens()) {
        		    count(tokenizer.nextToken());
        		}
    	    }
    	}
    }

    public Node(InitMessage initMessage) {
        nodeName = initMessage.nodeName;
        System.out.println("initNode :" + initMessage.nodeName + " " + initMessage.isRoot + " " + initMessage.isLeaf);
        System.out.println("\t" + "start : " + initMessage.add);
        this.neighbours = initMessage.neighbours;
        this.isRoot = initMessage.isRoot;
        this.add = initMessage.add;
        this.isLeaf = initMessage.isLeaf;
        subscribe(startHandler, control);
        subscribe(reportHandler,recievePort);
        subscribe(mergeHandler,recievePort);
    }


}

