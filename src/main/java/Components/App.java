package Components;


import Events.InitMessage;
import Ports.EdgePort;
import misc.Edge;
import se.sics.kompics.Channel;
import se.sics.kompics.Component;
import se.sics.kompics.ComponentDefinition;
import se.sics.kompics.Kompics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Stream;


public class App extends ComponentDefinition {

    String startNode = "";
    ArrayList<Edge> edges = new ArrayList<>();
    Map<String,Component> components = new HashMap<String,Component>();
    HashSet<String> nodes = new HashSet<String>();
    Map<String, Integer> countByWords = new HashMap<String, Integer>();

    public App(){
        readTable();
    }

    public static void main(String[] args) throws InterruptedException{
        Kompics.createAndStart(App.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.exit(1);
        }
        Kompics.shutdown();
//        Kompics.waitForTermination();

    }
    
    private String getSource() {
        int[][] dist = new int[nodes.size()][nodes.size()];
        HashMap<String, Integer> dict = new HashMap<String, Integer>();
        int counter = 0;
        for(String i : nodes) {
        	dict.put(i, counter++);
        }
    	for(int i = 0; i < nodes.size(); i++) {
    		for(int j = 0; j < nodes.size(); j++) {
    			dist[i][j] = Integer.MAX_VALUE;
    		}
    	}
        for(int i = 0; i < nodes.size(); i++) {
        	dist[i][i] = 0;
        }
        for(Edge e : edges) {
        	dist[dict.get(e.src)][dict.get(e.dst)] = e.weight;
        }
        for(int k = 0; k < nodes.size(); k++) {
        	for(int i = 0; i < nodes.size(); i++) {
        		for(int j = 0; j < nodes.size(); j++) {
        			if(dist[i][j] > dist[i][k] + dist[k][j]) {
        				dist[i][j] = dist[i][k] + dist[k][j];
        			}
        		}
        	}
        }
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for(int i = 0; i < nodes.size(); i++) {
        	int sum = 0;
    		for(int j = 0; j < nodes.size(); j++) {
    			sum += dist[i][j];
    		}
    		if(sum < min) {
    			min = sum;
    			minIndex = i;
    		}
    	}
        for(String i : dict.keySet()) {
        	if(dict.get(i) == minIndex) {
        		return i;
        	}
        }
        return "";
    }
    
   private int getNumOfLeaves() {
	   int result = 0;
	   for(String i : nodes) {
		  if(countByWords.get(i) == 1) {
			  result++;
		  }
	   }
	   return result;
   }
   
   private boolean isLeaf(String i) {
	  if(countByWords.get(i) == 1) {
		return true;  
	  }
	  return false;
   }
   
   private void count(String src) {
	   Integer count = countByWords.get(src);
       if (count != null) {
           countByWords.put(src, count + 1);
       } else {
           countByWords.put(src, 1);
       }
   }
   
   private int getInputLen() throws IOException {
	   try (Stream<String> fileStream = Files.lines(Paths.get("src/main/java/CA-text-file.txt"))) {
		   return (int) fileStream.count();
	   }
   }
   
   private void readFile() throws IOException {
   	try (Stream<String> lines = Files.lines(Paths.get("src/main/java/CA-text-file.txt"),
   			StandardCharsets.UTF_8))
   	{
   		int numOfLeaves = getNumOfLeaves();
        System.out.println(numOfLeaves);
        int len = getInputLen();
        int step = len/numOfLeaves;
        int count = 0;
        int i = 0;
        boolean end = false;
        FileWriter fileWriter = new FileWriter("target/" + count + ".txt");
		PrintWriter printWriter = new PrintWriter(fileWriter);
   	    for (String line : (Iterable<String>) lines::iterator)
   	    {
   	    	if(count == numOfLeaves-1) {
   	    		end = true;
   	    	}
   	    	printWriter.print(line);
   	    	i++;
   	    	if(i% step == 0 && end == false) {
   	    		count++;
   			    printWriter.close();
   			    fileWriter = new FileWriter("target/" + count + ".txt");
   			    printWriter = new PrintWriter(fileWriter);
   	    	}
   	    }
   	    if(end) {
   	    	printWriter.close();
   	    }
   	}
   }

    public void readTable(){
        File resourceFile = new File("src/main/java/tables.txt");
        try (Scanner scanner = new Scanner(resourceFile)) {
            int i = 0;
            while (scanner.hasNext()){
                String line = scanner.nextLine();
	            if (line.split(",").length > 1){
	                int weight = Integer.parseInt(line.split(",")[1]);
	                String rel = line.split(",")[0];
	                String src = rel.split("-")[0];
	                String dst = rel.split("-")[1];
	                nodes.add(src);
	                nodes.add(dst);
	                count(src); count(dst);
	                edges.add(new Edge(src,dst,weight));
	            }
            }
            System.out.println("ali");
            startNode =  getSource();
            readFile();
            int count = 0;
            for(String in : countByWords.keySet()) {
            	System.out.println(in + " : " + countByWords.get(in));
            }
            for(Edge edge:edges){
                if (!components.containsKey(edge.src)){
                     Component c = create(Node.class,new InitMessage(edge.src,edge.src.equalsIgnoreCase
                             (startNode), isLeaf(edge.src), count+".txt",findNeighbours(edge.src)));
                     components.put(edge.src,c) ;
                     if(isLeaf(edge.src)) {
                 		count++;
                 	}
                }
                if (!components.containsKey(edge.dst)){
                	Component c = create(Node.class,new InitMessage(edge.dst,edge.dst.equalsIgnoreCase
                            (startNode), isLeaf(edge.dst), count+".txt",findNeighbours(edge.dst)));
                    components.put(edge.dst,c) ;
                    if(isLeaf(edge.dst)) {
                		count++;
                	}
                }
                connect(components.get(edge.src).getPositive(EdgePort.class),
                        components.get(edge.dst).getNegative(EdgePort.class), Channel.TWO_WAY);
                connect(components.get(edge.src).getNegative(EdgePort.class),
                        components.get(edge.dst).getPositive(EdgePort.class),Channel.TWO_WAY);
            }


            System.out.println(startNode);


        } catch (IOException e) {
            e.printStackTrace();
        }




    }
    private HashMap<String,Integer> findNeighbours(String node){
        HashMap<String,Integer> nb = new HashMap<String,Integer>();
        for(Edge tr:edges){
            if(tr.src.equalsIgnoreCase(node) && !nb.containsKey(tr.dst)){
                nb.put(tr.dst , tr.weight);
            }
            else if (tr.dst.equalsIgnoreCase(node) && !nb.containsKey(tr.src)){
                nb.put(tr.src , tr.weight);
            }
        }
        return nb;
    }
}
