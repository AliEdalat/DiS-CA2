package Events;


import misc.TableRow;
import se.sics.kompics.KompicsEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReportMessage implements KompicsEvent {

    public String dst;
    public String src;
    public Map<String, Integer> countByWords;

	public ReportMessage(String src, String dst, Map<String, Integer> countByWords) {
		this.dst = dst;
        this.src = src;
        this.countByWords = countByWords;
	}
}
