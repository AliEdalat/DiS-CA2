package Events;

import java.util.ArrayList;

import misc.TableRow;
import se.sics.kompics.KompicsEvent;

public class MergeMessage implements KompicsEvent {

    public String dst;
    public String src;
    public ArrayList<TableRow> route_table;

    public MergeMessage( String src, String dst, ArrayList<TableRow> route_table) {
        this.dst = dst;
        this.src = src;
        this.route_table = route_table;
    }
	
}
