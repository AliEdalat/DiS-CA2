package Ports;

import Events.MergeMessage;
import Events.ReportMessage;
import se.sics.kompics.PortType;

public class EdgePort extends PortType {{
    positive(MergeMessage.class);
    positive(ReportMessage.class);
    negative(MergeMessage.class);
    negative(ReportMessage.class);
}}
